package Agenda;

import Pessoa.Pessoa;
import TADArvore.Arvore;
import TADArvore.No;
/**
 * 
 * @author sarah
 *
 */
public class Agenda {
	
	Arvore arvore = new Arvore();
	/**
	 * 
	 * @param pessoa
	 */
	public void armazenaPessoa(Pessoa pessoa){
		arvore.inserir(2, pessoa);
	}
	/**
	 * Remove a pessoa com nome passado por parâmetro
	 * @param nome
	 */
	public void removePessoa(String nome){
		arvore.remover(nome);
	}
	/**
	 * @param nome
	 * @return posição da agenda está a	pessoa
	 */
	public int buscaPessoa(String nome){
		int index = 0;
		return index;
	}
	/**
	 * imprime os dados de todas as pessoas da agenda
	 */
	public void imprimeAgenda(boolean i){
		arvore.imprimir(i);
	}
	/**
	 * Imprime os dados da pessoa com este nome.
	 * @param nome
	 */
	public void imprimePessoa(String nome){
		Pessoa p = (Pessoa) arvore.buscar(nome).getValor();
		System.out.println(p.mostrarInfos());
	}

}
