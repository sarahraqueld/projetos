package Agenda;

import java.time.LocalDate;

import Pessoa.Pessoa;

public class App {

	public static void main(String[] args) {
		LocalDate data = LocalDate.of(1996, 06, 30);
		Agenda a1 = new Agenda();
		Pessoa p1 = new Pessoa("Sarah", data, "09743701427", "99267186");
		a1.armazenaPessoa(p1);
		a1.imprimeAgenda(true);
		System.out.println("OK");

	}

}
