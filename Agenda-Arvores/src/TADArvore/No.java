package TADArvore;

/**
 * 
 * @author sarah
 *
 */
public class No 
{
	private int chave;
	private Object valor;
	private No esquerda;
	private No direita;
	
	public No(int chave, Object valor)
	{
		this.chave = chave;
		this.valor = valor;
		this.esquerda = null;
		this.direita = null;
	}
	
	public int getChave()
	{
		return this.chave;
	}
	
	public Object getValor()
	{
		return this.valor;
	}
	
	public void setValor(Object valor)
	{
		this.valor = valor;
	}
	
	public No getDireita()
	{
		return this.direita;
	}
	
	public void setDireita(No direita)
	{
		this.direita = direita;
	}
	
	public No getEsquerda()
	{
		return this.esquerda;
	}
	
	public void setEsquerda(No esquerda)
	{
		this.esquerda = esquerda;
	}
}
