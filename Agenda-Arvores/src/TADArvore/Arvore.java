package TADArvore;
/**
 * 
 * @author sarah
 *
 */
public class Arvore
{
	private No raiz;

	public Arvore()
	{	
	}
	//FALTA IMPLEMENTAR
	// Método externo que recebe uma String e gera uma chave inteira que ser� usada para buscar o N�
	public No buscar(String nome)
	{
		
		// gerar chave do valor
		int chave = 0;
		return buscar(chave);
	}

	// M�todo interno que recebe a chave gerada pelo valor para inicializar a busca
	private No buscar(int chave)
	{
		return buscar(raiz, chave);
	}

	// M�todo interno para percorrer com base na raiz
	private No buscar(No no, int chave)
	{
		// Se a chave buscada n�o existe
		if (no == null)
		{
			return null;
		}
		// Se a chave for maior que a chave do n� atual, busca na subArvore a direita
		if (chave > no.getChave())
		{
			return buscar(no.getDireita(), chave);
		}
		// Se a chave for menor que a chave do n� atual, busca na subArvore a esquerda
		if (chave < no.getChave())
		{
			return buscar(no.getEsquerda(), chave);
		}
		// Se n�o for nulo, nem maior, nem igual, n� encontrado, retorna o N� atual
		return no;
	}

	// Método externo com as informações do nó que será inserido
	public void inserir(int chave, Object valor)
	{
		this.raiz = inserir(this.raiz, chave, valor);
	}

	// M�todo interno com as infos do nó a ser inserido quer percorre come�ando pela raiz da �rvore
	private No inserir(No no, int chave, Object valor)
	{
		// Se o n� atual for nulo, lugar encontrado, cria o n� com as infos passadas
		if (no == null)
		{
			return new No(chave, valor);
		}
		// Se a chave passada for maior que a chave do n� atual, chama a subArvore a direita
		else if (chave > no.getChave())
		{
			no.setDireita(inserir(no.getDireita(), chave, valor));
		}
		// Se a chave passada for menor que a chave do n� atual, chama a subArvore a esquerda
		else if (chave < no.getChave())
		{
			no.setEsquerda(inserir(no.getEsquerda(), chave, valor));
		}
		// Se n�o for menor, nem maior, nem nulo, chave j� existe e o valor � atualizado
		else
		{
			no.setValor(valor);
		}
		// Se n�o tiver sido criado um novo n�, retorna o n� que teve o valor atualizado
		return no;
	}

	// M�todo externo que recebe uma String e gera uma chave inteira que ser� usada para buscar o N�
	public No remover(String nome)
	{
		// gerar chave do valor
		int chave = 0;
		return remover(chave);
	}

	// M�todo interno que recebe a chave gerada pelo valor para inicializar a remo��o
	private No remover(int chave)
	{
		return remover(this.raiz, chave);
	}

	// M�todo interno para buscar e remover o n� com a chave informada
	private No remover(No no, int chave)
	{
		// Se a chave for menor que a chave do n� atual, chama a subArvore a esquerda
		if (chave < no.getChave())
		{
			return remover(no.getEsquerda(), chave);
		}
		// Se a chave for maior que a chave do n� atual, chama a subArvore a direita
		else if (chave > no.getChave())
		{
			return remover(no.getDireita(), chave);
		}
		// Se a chave for igual, n� encontrado, come�a o processo de remo��o
		else if (chave == no.getChave())
		{
			/*
			 * Se a �rvore a esquerda for nula, basta atribuir a subArvore
			 * direita ao n� atual Retorna o n� removido
			 */
			if (no.getEsquerda().equals(null))
			{
				No removido = no;
				no = no.getDireita();
				return removido;
			}
			/*
			 * Se a �rvore direita for nula, basta atribuir a subArvore esquerda
			 * ao n� atual Retorna o n� removido
			 */
			else if (no.getDireita().equals(null))
			{
				No removido = no;
				no = no.getEsquerda();
				return removido;
			}
			/*
			 * Se nenhuma das duas subArvores forem nulas, busca-se a maior
			 * chaves das menores (subArvore esquerda) Porque eu quis, poderia
			 * ser tb a menor das chaves maiores (subArvore direita), s� teria
			 * que mudar uns detalhes Ao encontrar a chave que vai substituir,
			 * primeiro corrige o vazio que vai gerar Depois atualiza os filhos
			 * do no substituto e depois atualiza o no atual Retorna o n�
			 * removido
			 */
			else
			{
				No removido = no;
				No maiorEsquerda = encontrarMaior(no.getEsquerda());
				corrigirVazio(no.getEsquerda(), maiorEsquerda);
				maiorEsquerda.setDireita(no.getDireita());
				maiorEsquerda.setEsquerda(no.getEsquerda());
				no = maiorEsquerda;
				return removido;
			}
		}
		return null;
	}

	/*
	 * M�todo interno para corrigir o vazio gerado pela remo��o do n� que ir�
	 * substituir o n� removido Como a �rvore n�o tem ponteiro para o pai,
	 * percorro ela novamente procurando o filho que vai ser removido Ao
	 * encontrar, atualiza o ponteiro do pai que apontava para o filho com o
	 * ponteiro da esquerda do filho
	 */
	private void corrigirVazio(No no, No maior)
	{
		/*
		 * Se a chave do filho a direita for a chave do maior n� (que j� foi
		 * previamente encontrado) Atualiza o ponteiro direito do pai com o
		 * ponteiro esquerdo do filho
		 */
		if (no.getDireita().getChave() == maior.getChave())
		{
			no.setDireita(maior.getEsquerda());
		}
		// Se n�o continua descendo sempre pela direita
		else
		{
			corrigirVazio(no.getDireita(), maior);
		}
	}

	/*
	 * M�todo interno para encontrar o maior n� de uma subArvore e retorna-lo
	 */
	private No encontrarMaior(No no)
	{
		// Se o n� a direita for nulo ent�o o n� atual � o maior n�
		if (no.getDireita().equals(null))
		{
			return no;
		}
		// Se n�o, continua a busca pela direita
		else
		{
			return encontrarMaior(no.getDireita());
		}
	}

	/**
	 * Método interno para encontrar o menor nó de uma subArvore e retorná-lo
	 * @param no
	 * @return
	 */
	private No encontrarMenor(No no)
	{
		// Se o n� a esquerda for nulo, ent�o o n� atual � o menor n�
		if (no.getEsquerda().equals(null))
		{
			return null;
		}
		// Se n�o, continua a busca pela esquerda
		else
		{
			return encontrarMenor(no.getEsquerda());
		}
	}
	/**
	 * Retorna a profundidade da árvore
	 * @param root
	 * @param no
	 * @return
	 */
	private int getProfundidade(No root, No no)
	{
		if (no.getChave() < root.getChave())
		{
			return getProfundidade(root.getEsquerda(), no) + 1;
		}
		if (no.getChave() > root.getChave())
		{
			return getProfundidade(root.getDireita(), no) + 1;
		}
		return 1;
	}
	/**
	 * Recebe um nó e imprime o valor de sua chave, além de buscar pelos próximos nós, de acordo com o caminho pré fixado
	 * @param no
	 */
	private void preFixo(No no)
	{
		if (!no.equals(null))
		{
			System.out.println(no.getChave() + " ");
			preFixo(no.getEsquerda());
			preFixo(no.getDireita());
		}
	}
	/**
	 * Recebe um nó e imprime o valor de sua chave, além de buscar pelos próximos nós, de acordo com o caminho pós fixado
	 * @param no
	 */
	private void posFixo(No no)
	{
		if (!no.equals(null))
		{
			posFixo(no.getEsquerda());
			posFixo(no.getDireita());
			System.out.println(no.getChave() + " ");
		}
	}
	/**
	 * Recebe um valor true ou false, em que true corresponde a caminho pré-fixado e false, caminho pós fixado
	 * @param i
	 */
	public void imprimir(boolean i){
		if(i){
			preFixo(this.raiz);
			return;
		}
		posFixo(raiz);
	}
}
