package Pessoa;

import java.time.LocalDate;
import java.time.Period;
/**
 * 
 * @author sarah
 *
 */
public class Pessoa
{
	private String nome;
	private LocalDate nascimento;
	private String cpf;
	private String telefone;
	
	public Pessoa(String nome, LocalDate nascimento, String cpf, String telefone){
		this.nome = nome;
		this.nascimento = nascimento;
		this.cpf = cpf;
		this.telefone = telefone;
	}

	public String getNome()
	{
		return nome;
	}

	public void setNome(String nome)
	{
		this.nome = nome;
	}

	public LocalDate getNascimento()
	{
		return nascimento;
	}

	public void setNascimento(LocalDate nascimento)
	{
		this.nascimento = nascimento;
	}

	public String getCpf()
	{
		return cpf;
	}

	public void setCpf(String cpf)
	{
		this.cpf = cpf;
	}

	public String getTelefone()
	{
		return telefone;
	}

	public void setTelefone(String telefone)
	{
		this.telefone = telefone;
	}

	public String mostrarInfos()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("Nome: ");
		sb.append(this.nome);
		sb.append("\nData de nascimento: ");
		sb.append(this.nascimento.toString());
		sb.append("\nCPF: ");
		sb.append(this.cpf);
		sb.append("\nTelefone: ");
		sb.append(this.telefone);
		return sb.toString();
	}

	public int calcularIdade()
	{
		return Period.between(this.nascimento, LocalDate.now()).getYears();
	}
}
