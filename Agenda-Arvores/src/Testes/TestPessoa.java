package Testes;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import org.junit.Test;
import Pessoa.Pessoa;

public class TestPessoa 
{
	@Test
	public void testIdade()
	{
		Pessoa p = new Pessoa();
		LocalDate ld = LocalDate.of(1995, 04, 29);
		p.setNascimento(ld);
		System.out.println(p.calcularIdade());
		assertEquals(21, p.calcularIdade());
	}
}
