//Simulador de Escalonamento de Processos
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <signal.h>
#include <sys/time.h>
#include <errno.h>
//#include "BlackGPIO/BlackGPIO.h"
#include <unistd.h>
#include "escalonador.h"


int main (void) {
/*	
	// Declara o led RGB
	BlackLib::BlackGPIO 
	ledR(BlackLib::GPIO_60,BlackLib::output, BlackLib::SecureMode);
	BlackLib::BlackGPIO 
	ledG(BlackLib::GPIO_50,BlackLib::output, BlackLib::SecureMode);
	BlackLib::BlackGPIO 
	ledB(BlackLib::GPIO_51,BlackLib::output, BlackLib::SecureMode);
*/
  	
  	 // Declara o botão switch
 	/*BlackLib::BlackGPIO  
  	botao1(BlackLib::GPIO_115, BlackLib::input, BlackLib::SecureMode);
  	
  	bool sair = false;
 	bool iniciou = false;
  	
  	while(sair == false){
    if(botao1.getNumericValue() ==  1){
      // Se ele foi apertado e o programa ainda não iniciou, o programa inicia
      if(iniciou == false){
        cout << "Escalonando processos..." << endl;
        iniciou = true;
      }
      //Se foi apertado e o programa já tiver sido iniciado, encerra o programa
      else if(iniciou == true){
        sair = true;
        cout << "Programa encerrado pelo usuário." << endl;
        return 0;
      }
    }
    */

    struct processos *plist, *ptmp;
    plist       = init_processos(1, 10, 3);			//numero do processo, prioridade e tempo;
    plist->prox = init_processos(2,  1, 1); ptmp = plist->prox;
    ptmp->prox  = init_processos(3,  2, 3); ptmp = ptmp->prox;
    ptmp->prox  = init_processos(4,  1, 4); ptmp = ptmp->prox;
    ptmp->prox  = init_processos(5,  5, 2);
    /* Simulações executadas*/
    listprocs(plist);
    fcfs(plist);     
    prioridade(plist);
    rr(plist,0);

     // Instalar o tratamento do alarme
    signal(SIGALRM,detectarAlarme);
    
    while (plist != NULL) {
        ptmp = plist;
        plist = plist->prox;
        free(ptmp);
    };
    
    signal(SIGALRM, fim);

    return(0);

};

