#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <signal.h>
#include <sys/time.h>
#include <errno.h>
//#include "BlackGPIO/BlackGPIO.h"
#include <unistd.h>
#include "escalonador.h"


void detectarAlarme (int sig){

  printf("O sinal %d foi recebido: SIGALRM\n", sig);
 // printf("O placar atual de escalonamento é: %s\n", );
}

void fim(int sig){
  //kill all
}

// Inicialização de entrada da lista de processos
struct processos *init_processos (int id, int tempo, int prioridade) {
    struct processos *proc;
 proc = (struct processos*)malloc(sizeof(struct processos)); 
  if (proc == NULL) {
        printf("Erro Fatal: Falha na Alocacao de memoria.\nFinalizar.\n");
        exit(1);
    };
    proc->id = id;
    proc->tempo = tempo;
    proc->prioridade = prioridade;
    proc->execucao = 0;
    proc->espera = 0;
    proc->prox = NULL;
    return(proc);
};

// Escalonamento FCFS - o primeiro que chega é o primeiro que sai;
//Será escalonado primeiro;
void fcfs (struct processos *proc) {
    int time = 0, inicio, fim;
  struct processos *tmp = proc;
  printf("\tEscalonamento FCFS\n");
    printf("\n");
  while (tmp != NULL) {
    inicio = time;
    time += tmp->tempo;
    fim = time;
    printf("Processo: %d\tTempo de Execucao: %d\tEspera: %d\tRetorno: %d\n", tmp->id, time, inicio, fim);
    tmp = tmp->prox;
  };
    printf("\n\n");
};

// Listando Processos 
void listprocs (struct processos *proc) {
  struct processos *tmp = proc;
  printf("\tListagem de Processos\n");
  printf("\n");
  while (tmp != NULL) {
    printf("Processo: %d\tPrioridade: %d\tTempo: %d\n", tmp->id, tmp->prioridade, tmp->tempo);
    tmp = tmp->prox;
  };
  printf("\n\n");
 };

 /* Simulação de Processos por Prioridade
 *  O processo de menor valor de prioridade obtem
 *  prioridade maior na fila de processos */
void prioridade (struct processos *proc) {
  int time, inicio, fim, maior;
  struct processos *copia, *tmpsrc, *tmp, *maiorprimeiro;
  printf("\tEscalonamento por Prioridade\n");
   printf("\n");
  
     /* Replicando Lista de Processos */
  tmpsrc = proc;
  copia = tmp = NULL;
  while (tmpsrc != NULL) {
    if (copia == NULL) {
    copia = init_processos(tmpsrc->id, tmpsrc->tempo, tmpsrc->prioridade);
    tmp = copia;
    } else {
    tmp->prox = init_processos(tmpsrc->id, tmpsrc->tempo, tmpsrc->prioridade);
    tmp = tmp->prox;
    };
    tmpsrc = tmpsrc->prox;
  };
  /* Programa Principal */
  time = 0;
  while (copia != NULL) {
    
    /* Localiza o proximo processo */
    maiorprimeiro = NULL;
    maior = copia->prioridade;
    tmp = copia->prox;
    tmpsrc = copia;
    while (tmp != NULL) {
    if (tmp->prioridade < maior) {
      maior = tmp->prioridade;
      maiorprimeiro = tmpsrc;
    };
    tmpsrc = tmp;
    tmp = tmp->prox;
    };
     if (maiorprimeiro == NULL) {
    /* Verifica se o primeiro processo possui maior prioridade */
    inicio = time;
    time += copia->tempo;
    fim = time;
    printf("Processo: %d\tTempo: %d\tEspera: %d\tRetorno: %d\n", copia->id, time, inicio, fim);
    tmpsrc = copia->prox;
    free(copia);
    copia = tmpsrc;
    } else {
    /* Verifica se o primeiro processo não possui maior prioridade */
    tmp = maiorprimeiro->prox;
    inicio = time;
    time += tmp->tempo;
    fim = time;
    printf("Processo: %d\tTempo: %d\tEspera: %d\tRetorno: %d\n", tmp->id, time, inicio, fim);
    maiorprimeiro->prox = tmp->prox;
    free(tmp);
    };
  };
  printf("\n\n");
};

/* Escalonamento Round-Robin */
void rr (struct processos *proc, int quantum) {
  int jobsremain, passes;
  struct processos *copia, *tmpsrc, *tmp, *slot;
  printf("\tEscalonamento Round-Robin - Quantum: %d)\n", quantum);
  printf("\n");
  tmpsrc = proc;
  copia = tmp = NULL;
  while (tmpsrc != NULL) {
    if (copia == NULL) {
    copia = init_processos(tmpsrc->id, tmpsrc->tempo, tmpsrc->prioridade);
    tmp = copia;
    } else {
    tmp->prox = init_processos(tmpsrc->id, tmpsrc->tempo, tmpsrc->prioridade);
    tmp = tmp->prox;
    };
    tmpsrc = tmpsrc->prox;
  };
  /* Programa rotina de análise de prioridade  */
  jobsremain = 2;
  slot = NULL;
  while (jobsremain) {
    jobsremain = 0;
    /* Seleciona o próximo processo efetuando sua alocação */
    if (slot == NULL) {
    slot = copia;
    jobsremain = 2;
    } else {
    passes = 0;
    do {
      if (slot->prox == NULL) {
        passes++;
        slot = copia;
      } else {
        slot = slot->prox;
      };
    } while (passes <= 3 && slot->tempo == slot->execucao);
    if (passes <= 3) {
      jobsremain = 2;
    };
    };
    /* Executa um ciclo */
    tmp = copia;
    while (tmp != NULL) {
    if (tmp->tempo > tmp->execucao) {
      if (tmp == slot) {
        tmp->execucao += quantum;
      } else {
        tmp->espera += quantum;
      };
    };
    tmp = tmp->prox;
    };
  };
  /* Exibe os resultados e elimina as replicações */
  tmp = copia;
  while (tmp != NULL) {
    printf("Processo: %d\tTempo: %d\tEspera: %d\tRetorno: %d\n", tmp->id, tmp->tempo, tmp->espera, tmp->execucao + tmp->espera);
    tmpsrc = tmp;
    tmp = tmp->prox;
    free(tmpsrc);
  };
  printf("\n");
      
}
