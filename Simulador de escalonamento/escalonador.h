#ifndef ESCALONADOR_H_
#define ESCALONADOR_H_

/* Estrutura dos Processos */
struct processos {
    int id;                   //Identifição do processo            
 	int tempo;                //Tempo de duração do processo
    int prioridade;           //prioridade do process
    int execucao;             //Tempo de execução do processo
    int espera;               // Tempo de espera do processo
    struct processos *prox;
};


// Declarações de Protótipos de funções
struct processos *init_processos (int id, int tempo, int prioridade);
//funçao para o algoritmo FCFS
void fcfs (struct processos *proc);
//funçao para listar os processos
void listprocs (struct processos *proc);
//funçao para verificar prioridade
void prioridade (struct processos *proc);
//funçao para o algoritmo Round-Robin
void rr (struct processos *proc, int quantum);
//funçao para o uso de alarme
void detectarAlarme (int sig);
void fim(int sig);

#endif
