#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <cstdlib>
#include <csignal>

int main()
{
   	pid_t filho;
   	filho = fork();

   	pid_t filho2;
   	pid_t neto1;
   	pid_t filho3;
   	pid_t neto2;

 	int anos = 0;
 	int anosF = 0;
 	bool sair = false;
 	int cont = 0;


   //criaçao do filho 01
 	while (!sair){
 		if (anos == 18){

   			if (filho == -1)
       			printf("Erro fork() %d: %s\n", errno, strerror(errno));
   			else
   			{
     			if (filho == 0)
        			printf(" Sou o Processo-filho 1: ID = %ld, Pai ID = %ld\n", (long)getpid(), (long)getppid());

    		 	else{
        			printf("Sou o Processo-pai: ID = %ld, Pai ID = %ld, Filho1 ID = %ld\n", (long)getpid(), (long)getppid(), (long)filho);
   					sleep(1);
   					if (cont++==5) kill(filho,SIGKILL);  //finalizar o processo filho
                }
            }
 		}
   		//criaçao do filho 02
   		if (anos == 20){
   			filho2 = fork();

   			if (filho2 == -1){
       			printf("Erro fork() %d: %s\n", errno, strerror(errno));
   			}
   			else(filho2 == 0)
   			{
                printf(" Sou o Processo-filho 2: ID = %ld, Pai ID = %ld\n", (long)getpid(), (long)getppid());

                if (anosF == 15){
                    //Criaçao do processo neto 1;
                    neto1 = fork ();
                    if (neto1 == -1){
                        printf("Erro fork() %d: %s\n", errno, strerror(errno));
                    }
                    else{
                        if (neto1 == 0)
                            printf(" Sou o Processo-neto 1: ID = %ld, Pai ID = %ld\n", (long)getpid(), (long)getppid());
                        else
                            printf("Sou o Processo-pai: ID = %ld, Pai ID = %ld, Filho1 ID = %ld\n", (long)getpid(), (long)getppid(), (long)filho);
                            sleep(1);
                        if (cont++==5) kill(neto1,SIGKILL);  //finalizar o processo neto
                    }
                }

            }
        }
        else{
            printf("Sou o Processo-pai: ID = %ld, Pai ID = %ld, Filho2 ID = %ld\n", (long)getpid(), (long)getppid(), (long)filho);
            sleep(1);
            if (cont++==20) kill(filho2,SIGKILL);  //finalizar o processo filho2
        }

		//criaçao do filho 03;
   		if (anos == 21){
   			filho3 = fork();

   			if (filho3 == -1){
       			printf("Erro fork() %d: %s\n", errno, strerror(errno));
   			}
   			else
   			{
     			if (filho3 == 0){
        			printf(" Sou o Processo-filho 3: ID = %ld, Pai ID = %ld\n", (long)getpid(), (long)getppid());

        			//Criaçao do processo neto 2;
        			if(anosF == 3){
        				neto2 = fork ();
        				if (neto2 == -1)
       						printf("Erro fork() %d: %s\n", errno, strerror(errno));
   						else
   						{
     						if (neto2 == 0)
        						printf(" Sou o Processo-neto 2: ID = %ld, Pai ID = %ld\n", (long)getpid(), (long)getppid());
    		 				else{
        						printf("Sou o Processo-pai: ID = %ld, Pai ID = %ld, Filho1 ID = %ld\n", (long)getpid(), (long)getppid(), (long)filho);
   								sleep(1);
                                if (cont++==10) kill(neto2,SIGKILL);  //finalizar o processo neto2
                            }
   						}
     				}
     			}
                else{
                    printf("Sou o Processo-pai: ID = %ld, Pai ID = %ld, Filho3 ID = %ld\n", (long)getpid(), (long)getppid(), (long)filho);
                    sleep(1);
                    if (cont++==5) kill(filho3,SIGKILL);  //finalizar o processo filho3
                }
   			}
        }

   		anos++;
   		anosF++;
   		sleep(1);

	}

return 0;

}
