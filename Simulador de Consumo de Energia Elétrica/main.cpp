// Includes do botão e led vermelho
#include "BlackGPIO/BlackGPIO.h"
#include <iostream>
#include <unistd.h>
//Includes da medição analógica
#include <fstream>
#include <string>
#include <sstream>
#include <chrono>

using namespace std;
//Medição analógica
#define PATH_ADC "/sys/bus/iio/devices/iio:device0/in_voltage"
int readAnalog(int number){
   stringstream ss;
   ss << PATH_ADC << number << "_raw";
   fstream fs;
   fs.open(ss.str().c_str(), fstream::in);
   fs >> number;
   fs.close();
   return number;
}


int main(int argc, char *argv[])
{
	// Declara o led vermelho
	BlackLib::BlackGPIO   
	led1(BlackLib::GPIO_50,BlackLib::output, BlackLib::SecureMode);
	// Declara o botão switch
	BlackLib::BlackGPIO  
	botao1(BlackLib::GPIO_115, BlackLib::input, BlackLib::SecureMode);
	// Declara o led RGB
	BlackLib::BlackGPIO 
	ledR(BlackLib::GPIO_60,BlackLib::output, BlackLib::SecureMode);
	BlackLib::BlackGPIO 
	ledG(BlackLib::GPIO_50,BlackLib::output, BlackLib::SecureMode);
	BlackLib::BlackGPIO 
	ledB(BlackLib::GPIO_51,BlackLib::output, BlackLib::SecureMode);

	// variável para controlar o fim do programa
	bool sair = false;
	// variável para verificar se o programa já iniciou ou não
	bool iniciou = false;
	// Variável que irá guardar o valor de consumo que o usuário quer gastar
	float consumoMaximo;
	// Variável que irá guardar o valor de consumo enquanto o programa está rodando
	long consumoAtual;

	/*SARAH: declara uma variavel de tempo, 
	inicia
	ultimotempo =  tempoinicial*/

	/* ANALLA: Imprime um menu dando instruções ao usuário, para passar um valor de consumo por parâmetro
	(mostrar exemplo) e dizendo que para iniciar a medição
	basta apertar o botão. Leia também o valor passado e atribua à variável consumoMaximo
	Caso o usuário não tiver passado valor de consumo por linha de comando, encerra o programa aqui
	*/


	std::chrono::high_resolution_clock::time_point ultimotempo;	
	
	while(!sair){
		cout << "Esperando botão" << endl;
		cout << botao1.getNumericValue() << endl;
		//Verifica se o botão switch foi apertado
		if(botao1.getNumericValue() ==  1){
			// Se ele foi apertado e o programa ainda não iniciou, o programa inicia
			if(iniciou == false){
				cout << "Insira o valor de consumo desejado." << endl;
				cin >> consumoMaximo;
				iniciou = true;
				led1.setValue(BlackLib::high);

				std::chrono::high_resolution_clock::time_point ultimotempo = std::chrono::high_resolution_clock::now();	
			}
			//Se foi apertado e o programa já tiver sido iniciado, encerra o programa
			else if(iniciou == true){
				sair = true;
				cout << "Programa encerrado pelo usuário. Seu consumo total foi de: " << consumoAtual << endl;
				return 0;
			}
		}
		if(iniciou == true){

			// resistencia
			int valor = readAnalog(1);
			//Starta o tempo
    		std::chrono::high_resolution_clock::time_point tempoatual = std::chrono::high_resolution_clock::now();
    		long double tempodecorrido = std::chrono::duration<long double, std::micro>( tempoatual - ultimotempo).count();
    		consumoAtual += ((valor*1.8)/8000)*((valor*1.8)/4096)*((valor*1.8)/4096) * tempodecorrido/(1000000*60);

			//Leitura da entrada analógica AIN1 (Potenciometro)
		
			cout << "Potência: " <<  ((valor*1.8)/8000)*((valor*1.8)/4096)*((valor*1.8)/4096)  << endl;
			cout << "Consumo atual (Kw/min)" << consumoAtual << endl;
			cout << "Valor (R$) : " << consumoAtual * 0.60 / 60 << endl;

			float umterco = 0.333 * consumoMaximo;
			float doistercos = 0.666 * consumoMaximo;

			if(consumoAtual <= umterco){
				ledR.setValue(BlackLib::low);
				ledG.setValue(BlackLib::high);
				ledB.setValue(BlackLib::low);
			}
			else if (consumoAtual > umterco && consumoAtual <= doistercos){
				ledR.setValue(BlackLib::low);
				ledG.setValue(BlackLib::low);
				ledB.setValue(BlackLib::high);
			}
			else if (consumoAtual > doistercos){
				ledR.setValue(BlackLib::high);
				ledG.setValue(BlackLib::low);
				ledB.setValue(BlackLib::low);
			}
			else{
				ledR.setValue(BlackLib::low);
				ledG.setValue(BlackLib::low);
				ledB.setValue(BlackLib::low);
			}

			std::chrono::high_resolution_clock::time_point ultimotempo = std::chrono::high_resolution_clock::now();	
			if(consumoAtual >= consumoMaximo){
				cout << "Seu consumo chegou ao máximo. O programa foi encerrado." << endl;
				return 0;
			}
			sleep(1);
		}
			if(iniciou == false)
				sleep(0.005); 
		}

    return 0;
}